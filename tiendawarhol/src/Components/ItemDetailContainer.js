import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import ItemDetail from "./ItemDetail";
import { doc, getDoc, getFirestore } from "firebase/firestore"
import Spinner from "react-bootstrap/Spinner"; 


const ItemDetailContainer = () => {
  const [item, setItem] = useState({});
  const { detalleId } = useParams();
  const [loader, setLoader] = useState(true);



  useEffect(() => {
    const db = getFirestore();
    const dbQuery = doc(db, "items", detalleId);
    getDoc(dbQuery)
      .then((item) => setItem({ id: item.id, ...item.data() }))
      .catch((err) => console.log(err))
      .finally(() => setLoader(false));
  }, []);


return (
      <div>
        {loader ? (
          <Spinner animation="border" variant="primary" />
        ) : (
        <ItemDetail item={item}/>
        )}
      </div>
    );
  }

export default ItemDetailContainer;
