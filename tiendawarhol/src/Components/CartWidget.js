import carrito from "../img/icon-cart-48.png";
import { useCartContext } from "../CartContext";
import { Link } from "react-router-dom";

const CartWidget = () => {
  const { cartList } = useCartContext();
  const totalItems = cartList.reduce(
    (acc, item) => (acc = acc + item.count),
    0
  );
  
  return (
    <>
      <Link to="/cart" style={{ pointerEvents: !totalItems ? "none" : "" }}>
        <button type="button" className="btn btn-warning position-relative">
          <img src={carrito} />
          <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
            {totalItems}
          </span>
        </button>
      </Link>
    </>
  );
}

export default CartWidget;
