import Item from "./Item";

export default function ItemList({ items }) {
  return (
    <div style={{display:"flex", flexWrap: "wrap", justifyContent: "center"}}>
      {items.map((prod) => (<Item key={prod.id} prod={prod} />)
      )}
    </div>
  );
} 
