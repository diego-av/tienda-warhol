import React from "react";
import { Link } from "react-router-dom";

export const BuyButton = () => {
  return (
    <>
      <div className="buyButton">
        <Link to={"/"}>
          <button className="btn btn-outline-primary">
            Seguir comprando
          </button>
        </Link>
        <Link to={"/cart"}>
          <button className="btn btn-primary">Finalizar compra</button>
        </Link>
      </div>
    </>
  );
};
