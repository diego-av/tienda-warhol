import { useState } from "react";
import { useCartContext } from "../CartContext";
import { BuyButton } from "./BuyButton";
import ItemCount from "./ItemCount";


const ItemDetail = ({ item }) => {

    const [inputType, setInputType] = useState('itemCount');
    const { addToCart } = useCartContext();

    function handleInputType() {
        setInputType('buyButtons');
    }

    const onAdd = (count) => {
      addToCart({ ...item, count });
    };

  return (
    <div className="row">
      <div className="col">
        <img src={item.img} style={{width:"350px"}} />
      </div>
      <div className="col">
        <h1>{item.name}</h1>
        <h2>{item.category}</h2>
        <p>{item.price}</p>
        {inputType === "itemCount" ? (
          <ItemCount
            initial={1}
            stock={5}
            onAdd={onAdd}
            handleInputType={handleInputType}
          />
        ) : (
          <BuyButton />
        )}
      </div>
    </div>
  );
};
export default ItemDetail;
