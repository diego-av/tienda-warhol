import { useEffect, useState } from "react";
import ItemList from "./ItemList";
import {useParams} from "react-router-dom"
import Spinner from "react-bootstrap/Spinner" 
import {getFirestore, doc, where, collection, getDocs, query} from "firebase/firestore"

const ItemListContainer = ({ greeting = "saludo generico" }) => {
  const [items, setItems] = useState([]);
  const [loader, setLoader] = useState(true);
  const { id } = useParams();

  useEffect(() => {
    const db = getFirestore();
    const queryCollection = collection(db, "items");

    if (!id) {
      getDocs(queryCollection)
        .then((resp) => setItems(resp.docs.map((item) => ({ id: item.id, ...item.data() }))))
        .catch((err) => console.log(err))
        .finally(() => setLoader(false));
    } else {
      const queryCollectionFilter = query(queryCollection, where("category", "==", id));
      getDocs(queryCollectionFilter)
        .then((resp) => setItems(resp.docs.map((item) => ({ id: item.id, ...item.data() }))))
        .catch((err) => console.log(err))
        .finally(() => setLoader(false));
    }
  }, [id]);

    return (
      <div>
        <h1>{greeting}</h1>
        {loader ? (
          <Spinner animation="border" variant="primary" />
        ) : (
          <ItemList items={items} />
        )}
      </div>
    );
  }


export default ItemListContainer;