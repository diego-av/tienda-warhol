import { useState } from "react";

const ItemCount = ({ stock, initial, handleInputType, onAdd }) => {
  const [cant, setCant] = useState(initial);

  const sumar = () => {
    if (cant < stock) {
      setCant(cant + 1);
    }
  };
  const restar = () => {
    if (cant > 1) {
      setCant(cant - 1);
    }
  };

  function addToCart () {
    onAdd(cant);
    handleInputType();
  }

  return (
    <div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <button className="btn btn-success" onClick={sumar}>
          +
        </button>
        <p style={{ padding: "0 20px 0 20px" }}>{cant}</p>
        <button className="btn btn-danger" onClick={restar}>
          -
        </button>
      </div>
      <br></br>
      <div>
        <button className="btn btn-primary" onClick={addToCart}>
          Agregar al carrito
        </button>
      </div>
    </div>
  );
};

export default ItemCount 
