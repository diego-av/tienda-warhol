import Card from "react-bootstrap/Card"
import { Link } from "react-router-dom";

 const Item = ({prod}) => {
  return (

    <div>
      <Card style={{ width: "18rem" }}>
        <Card.Img variant="top" src={prod.img} />
        <Card.Body>
          <Card.Text>{prod.category}</Card.Text>
          <Card.Title>{prod.name}</Card.Title>
          <Card.Text>Precio ${prod.price}</Card.Text>
          <Link to={`/detalle/${prod.id}`}>
            <button className="btn btn-outline-primary">Detalle del producto</button>
          </Link>
        </Card.Body>
      </Card>
    </div>
 );
}

export default Item
