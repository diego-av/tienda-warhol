import CartWidget from "./CartWidget";
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Container from 'react-bootstrap/Container'
import { Link, NavLink } from "react-router-dom";


const NavBar = () => {
  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Link to="/">
          <img style={{width:"150px"}} src="https://pinturaswarhol.com/wp-content/uploads/2018/04/Logo-Warhol.png"/>
        </Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" style={{ padding: "20px", textDecoration: "none"}}/>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto"style={{ padding: "20px" }}> 
            <NavLink to="/categoria/Interior" style={{ padding: "20px", textDecoration: "none"}}>Latex Interior</NavLink>
            <NavLink to="/categoria/Exterior" style={{ padding: "20px", textDecoration: "none"}}>Latex exterior</NavLink>
            <NavLink to="/categoria/Línea decoración" style={{ padding: "20px", textDecoration: "none"}}>Línea decorativa</NavLink>
            <NavLink to="/categoria/Recubrimientos" style={{ padding: "20px", textDecoration: "none"}}>Recubrimiento</NavLink>
          </Nav>
        </Navbar.Collapse>
          <CartWidget />
      </Container>
    </Navbar>
  );
};

export default NavBar;
