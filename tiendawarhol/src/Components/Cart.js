import { Link } from "react-router-dom";
import { useCartContext } from "../CartContext";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { useState } from "react";
import { collection, getFirestore, addDoc } from "firebase/firestore";
import Col from "react-bootstrap/esm/Col";
import Row from "react-bootstrap/esm/Row";

export const Cart = () => {
  const { cartList, deleteCart, deleteItem } = useCartContext();
  const [userId, setUserId] = useState("");
  const db = getFirestore();

  const total = cartList.reduce(
    (tot, product) => (tot = tot + parseFloat(product.price) * product.count),
    0
  );

  const [buyer, setBuyer] = useState({
    name: "",
    address: "",
    phone: "",
    email: "",
  });

  function handleInputChange(e) {
    setBuyer({
      ...buyer,
      [e.target.name]: e.target.value,
    });
  }

  function newOrder(e) {
    e.preventDefault();
    let order = {};

    order.buyer = buyer;
    order.items = cartList.map((product) => {
      const id = product.id;
      const name = product.name;
      const price = product.price;
      const quantity = product.count;

      return { id, name, price, quantity };
    });
    
    order.total = total;

    const queryCollectionOrders = collection(db, "orders");

    setTimeout(() => {
      addDoc(queryCollectionOrders, order)
        .then((resp) => setUserId(resp.id))
        .catch((err) => console.log(err))
        .finally(deleteCart())
      
    }, 2000);
  }

  return (
    <div>
      <h2>Carrito</h2>
      <div className="container">
        <div className="row">
          <div className="col">
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Imagen</th>
                  <th scope="col">Producto</th>
                  <th scope="col">Precio</th>
                  <th scope="col">Unidades</th>
                </tr>
              </thead>
              {cartList.map((item) => (
                <tbody>
                  <tr>
                    <td scope="row">
                      <img src={item.img} style={{ width: "100px" }}></img>
                    </td>
                    <td>{item.name}</td>
                    <td>${item.price}</td>
                    <td>{item.count}</td>
                    <td>
                      <Button
                        variant="danger"
                        onClick={() => deleteItem(item.id)}
                      >
                        Eliminar
                      </Button>
                    </td>
                  </tr>
                </tbody>
              ))}
            </table>
          </div>
          {cartList.length ? (
            <>
              <div>
                <h3>Total: ${total}</h3>
                <Button variant="danger" onClick={deleteCart}>
                  Vaciar Carrito
                </Button>
              </div>
              <div>
                <br></br>

                <h3>Confirma tu compra</h3>
                <Form onSubmit={(e) => newOrder(e)}>
                  <Row>
                    <Col>
                      <Form.Group className="mb-3">
                        <Form.Label>Nombre completo</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Ingresar nombre"
                          name="name"
                          onChange={handleInputChange}
                          required
                        />
                      </Form.Group>
                    </Col>

                    <Col>
                      <Form.Group className="mb-3">
                        <Form.Label>Dirección</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Ingresar dirección"
                          name="address"
                          onChange={handleInputChange}
                          required
                        />
                      </Form.Group>
                    </Col>
                  </Row>

                  <Row>
                    <Col>
                      <Form.Group className="mb-3">
                        <Form.Label>Teléfono</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Ingresar teléfono"
                          name="phone"
                          onChange={handleInputChange}
                          required
                        />
                      </Form.Group>
                    </Col>

                    <Col>
                      <Form.Group className="mb-3">
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                          type="email"
                          placeholder="Ingresar email"
                          name="email"
                          onChange={handleInputChange}
                          required
                        />
                      </Form.Group>
                    </Col>
                  </Row>

                  <Button variant="primary" type="submit">
                    Confirmar compra
                  </Button>
                </Form>
              </div>
            </>
          ) : (
            <div>
              <div class="alert alert-success" role="alert">
                Tu ID de compra es: {userId}
              </div>
              <p>No hay productos en el carrito</p>
              <Link to={"/"}>
                <Button variant="primary">Volver a la Tienda</Button>
              </Link>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
