const productosWarhol = [
  {
    id: "1",
    category: "Interior",
    name: "Latex interior",
    price: "1200",
    img: "https://i.ibb.co/xY9vW22/latex-interior.png",
  },
  {
    id: "2",
    category: "Exterior",
    name: "Latex exterior 4 lts",
    price: "1350",
    img: "https://i.ibb.co/HzL6hn0/latex-interior-exterior.png",
  },
  {
    id: "3",
    category: "Recubrimientos",
    name: "Frentes impermeables",
    price: "3300",
    img: "https://i.ibb.co/xY9vW22/latex-interior.png",
  },
  {
    id: "4",
    category: "Línea decoración",
    name: "Latex interior almendra",
    price: "2500",
    img: "https://i.ibb.co/7yRZ8bX/decoracion-almendra.png",
  },
  {
    id: "5",
    category: "Línea decoración",
    name: "Latex interior azul",
    price: "2500",
    img: "https://i.ibb.co/yn9tVQF/decoracion-azul.png",
  },
  {
    id: "6",
    category: "Línea decoración",
    name: "Latex interior naranja",
    price: "2500",
    img: "https://i.ibb.co/mcmzCqv/decoracion-naranja.png",
  },
  {
    id: "7",
    category: "Línea decoración",
    name: "Latex interior rosa",
    price: "2500",
    img: "https://i.ibb.co/BfVCfjX/decoracion-rosa.png",
  },
  {
    id: "8",
    category: "Línea decoración",
    name: "Latex interior verde",
    price: "2500",
    img: "https://i.ibb.co/nf8Qy1h/decoracion-verde.png",
  },
  {
    id: "9",
    category: "Línea decoración",
    name: "Latex interior violeta",
    price: "2500",
    img: "https://i.ibb.co/7nDKq4V/decoracion-violeta.png",
  },
  {
    id: "10",
    category: "Interior",
    name: "Latex cielorrasos",
    price: "4500",
    img: "https://i.ibb.co/6rjq5qY/latex-interior-cielorrasos.png",
  },
  {
    id: "11",
    category: "Interior",
    name: "Latex antihongos",
    price: "2500",
    img: "https://i.ibb.co/JC5xS8r/latex-interior-antihongos.png",
  },
  {
    id: "12",
    category: "Exterior",
    name: "Latex exterior 20 lts",
    price: "1350",
    img: "https://i.ibb.co/HzL6hn0/latex-interior-exterior.png",
  },
];

export const promesaProductos = (id)=>{
    return new Promise((resolve) => {
  setTimeout(() => {
    const query = id ? productosWarhol.find((productosWarhol) => productosWarhol.id === id) : productosWarhol 
      resolve(query)
  }, 2000) 
})  
}

