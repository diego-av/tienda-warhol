import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyAUCPn9l-ak-2K9t08NiuO5jLs1sqTP8TM",
  authDomain: "tiendawarhol.firebaseapp.com",
  projectId: "tiendawarhol",
  storageBucket: "tiendawarhol.appspot.com",
  messagingSenderId: "139516492861",
  appId: "1:139516492861:web:d2c72b9494fbc3d44afe54",
};

const app = initializeApp(firebaseConfig);

export default function getFirestoreApp() {
  return app
}
