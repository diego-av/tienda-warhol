import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import NavBar from "./Components/NavBar";
import ItemListContainer from "./Components/ItemListContainer";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import ItemDetailContainer from "./Components/ItemDetailContainer";
import { Cart } from "./Components/Cart";
import CartContextProvider from "./CartContext";


function App() {
  return (
    <BrowserRouter>
      <CartContextProvider>
      <div className="App">
        <NavBar />
        <Routes>
          <Route path="/" element={<ItemListContainer greeting={"Tienda"} />} />
          <Route path="/categoria/:id" element={<ItemListContainer greeting={"Categoria"} />} />
          <Route path="/detalle/:detalleId" element={<ItemDetailContainer />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/*" element={<Navigate to="/" replace />} />
        </Routes>
      </div>
      </CartContextProvider>
    </BrowserRouter>
  );
}

export default App;
